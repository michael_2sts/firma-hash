import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class DescifrarYFirma {

    String algoritmo;
    Key publicKey;
    Key privateKey;
    byte[] mensaje_cifrado;
    byte[] firma;

    public DescifrarYFirma(String algoritmo, Key publicKey, Key privateKey, byte[] mensaje_cifrado, byte[] firma) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.mensaje_cifrado = mensaje_cifrado;
        this.firma = firma;
    }

    public void descifrarmensaje() throws NoSuchPaddingException, NoSuchAlgorithmException {
        try {

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            System.out.println("\n");
            //si no pongo esto no me sale "Mensaje cifrado:"
            System.out.println("Mensaje cifrado: ");
            System.out.println(new String(mensaje_cifrado));
            byte[] descifrado = cipher.doFinal(mensaje_cifrado);
            System.out.println("\n");
            System.out.println("Mensaje descifrado: ");
            System.out.println(new String(descifrado));

        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean comprobarFirma (String algoritmo) {
        try {
            Signature verificar = Signature.getInstance(algoritmo);
            verificar.initVerify((PublicKey) publicKey);
            verificar.update(mensaje_cifrado);
            return verificar.verify(firma);

        } catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }

    }

}
