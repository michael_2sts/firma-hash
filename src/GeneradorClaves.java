
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class GeneradorClaves {

    String algoritmo;
    Key publickey;
    Key privatekey;


    public GeneradorClaves(String algoritmo, Key publickey, Key privatekey) {
        this.algoritmo = algoritmo;
        this.publickey = publickey;
        this.privatekey = privatekey;
    }

    public void keys() {

        try {

            KeyPairGenerator keygen = KeyPairGenerator.getInstance(algoritmo);
            KeyPair keyPair = keygen.generateKeyPair();

            publickey = keyPair.getPublic();
            privatekey = keyPair.getPrivate();

            guardarClave(publickey, "publicKey");
            guardarClave(privatekey, "privateKey");

        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    private static void guardarClave(Key clave, String nombreFichero) throws IOException {
        byte[] codificarClave = clave.getEncoded();
        FileOutputStream fs = new FileOutputStream(nombreFichero);
        fs.write(codificarClave);
        fs.close();
    }

    public byte[] leerArchivo(String nombreFichero) throws IOException {

        File file = new File(nombreFichero);
        byte[] contenido = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(contenido);
        fis.close();

        return contenido;

    }

    public Key leerClavePublica(String nombreFichero) throws IOException {

        try {
            File file = new File(nombreFichero);
            byte[] contenido = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(contenido);
            fis.close();

            KeyFactory keyFactory = KeyFactory.getInstance(algoritmo);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(contenido);
            return keyFactory.generatePublic(keySpec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public Key leerClavePrivada(String nombreFichero) throws IOException {
        try {
            File file = new File(nombreFichero);
            byte[] contenido = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(contenido);
            fis.close();

            KeyFactory keyFactory = KeyFactory.getInstance(algoritmo);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(contenido);
            return keyFactory.generatePrivate(keySpec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }


}


