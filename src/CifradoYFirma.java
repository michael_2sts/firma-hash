
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;

public class CifradoYFirma {

    String algoritmo;
    Key publickey;
    Key privatekey;
    String mensaje;
    byte [] mensaje_cifrado;
    byte [] firma;


    public CifradoYFirma(String algoritmo, Key publickey, Key privatekey, String mensaje, byte[] mensaje_cifrado, byte[] firma) {
        this.algoritmo = algoritmo;
        this.publickey = publickey;
        this.privatekey = privatekey;
        this.mensaje = mensaje;
        this.mensaje_cifrado = mensaje_cifrado;
        this.firma = firma;
    }

    public void CifradoMensaje() {

        try {

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privatekey);
            byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes());
            guardarMensaje(mensajeCifrado, "mensajeCifrado");


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException |
                 InvalidKeyException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void FirmaMensaje(String algoritmo1, byte [] mensaje_cifrado) {
        try {

            Signature sig = Signature.getInstance(algoritmo1);
            sig.initSign((PrivateKey) privatekey);
            sig.update(mensaje_cifrado);

            byte [] signature = null;

            signature =sig.sign();

            guardarMensaje(signature,"firmaMensaje");


        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    private void guardarMensaje(byte[] mensaje, String nombreFichero) throws IOException {

        FileOutputStream fs = new FileOutputStream(nombreFichero);
        fs.write(mensaje);
        fs.close();
    }
}



