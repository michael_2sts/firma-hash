import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class main {
    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException {

        Scanner scl = new Scanner(System.in);

        //generar claves publicas y privadas

        GeneradorClaves g = new GeneradorClaves("RSA",null,null);
        g.keys();

        //leer el archivo creado y convertirlo a key desde byte[]

        Key publickey = g.leerClavePublica("publicKey");
        Key privatekey = g.leerClavePrivada("privateKey");


        //mensaje desde la terminal

        System.out.println("Introduce un mensaje: ");
        String mensaje = scl.nextLine();

        //cifrar el mensaje

        CifradoYFirma c = new CifradoYFirma("RSA",publickey,privatekey,mensaje,null,null);
        c.CifradoMensaje();

        //leer el archivo que crea el mensaje cifrado y convertirlo a byte[]

        byte[] mensaje_cifrado = g.leerArchivo("mensajeCifrado");

        //firmar mensaje con otro algoritmo

        c.FirmaMensaje("SHA256withRSA",mensaje_cifrado);

        //leer el archivo que crea la firma y convertirla a byte[]

        byte[] firma = g.leerArchivo("firmaMensaje");

        //descifrar el mensaje cifrado con la clave publica

        DescifrarYFirma d = new DescifrarYFirma("RSA",publickey,privatekey,mensaje_cifrado,firma);
        d.descifrarmensaje();

        //verificar la firma con el mensaje cifrado con la clave publica y el otro algoritmo

        boolean verificar = d.comprobarFirma("SHA256withRSA");

        //comprobar que coinciden

        System.out.println("\n");

        if (verificar){
            System.out.println("La firma coincide con el mensaje");
        }else{
            System.out.println("La firma no coincide con el mensaje");
        }


    }
}
